<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php
	$categories = get_the_category();
	$category_id = $categories[0]->cat_ID;
?>

<h2 class="related">Read More Success Stories</h2>
<section class="image-cards">
	<div class="feed">
		<?php
			$args = array(
				'post_type' => 'successstory',
				'posts_per_page' => 2,
				'order' => 'ASC',
				'post__not_in' => array( $post->ID ),
				'cat' => $category_id,
			);
			$loop = new WP_Query( $args );
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php get_template_part( 'template-parts/previews/preview-success' ); ?>
			<?php endwhile;?>
			<?php wp_reset_query(); ?>
	</div>
</section>