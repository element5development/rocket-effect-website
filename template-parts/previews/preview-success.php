<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<div class="card">
	<a href="<?php the_permalink(); ?>"></a>
	<?php $image = get_field('logo'); ?>
	<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	<h3><?php the_title(); ?></h3>
	<p><?php the_excerpt(); ?></p>
	<a class="button is-borderless is-ghost is-purple" href="<?php the_permalink(); ?>">View the Story</a>
</div>