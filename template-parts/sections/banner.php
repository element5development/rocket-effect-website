<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>

<?php if ( get_sub_field('description') ) : ?>
	<section class="banner has-description">
		<div>
			<h2><?php the_sub_field('title'); ?></h2>
			<div>
				<p><?php the_sub_field('description'); ?></p>
			</div>
			<div>
				<?php $link = get_sub_field('button'); ?>
				<a class="button is-ghost is-white" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			</div>
		</div>
	</section>
<?php else : ?>
	<section class="banner no-description">
		<div>
			<h2><?php the_sub_field('title'); ?></h2>	
			<?php $link = get_sub_field('button'); ?>
			<a class="button is-ghost is-white" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		</div>
	</section>
<?php endif; ?>