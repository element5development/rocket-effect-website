<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>


<main>

	<article>
			<section class="form">
				<?php if ( get_field('heading') ) : ?>
					<h2><?php the_field('heading'); ?></h2>
				<?php endif; ?>
				<?php if ( get_field('form_description') ) : ?>
					<p><?php the_field('form_description'); ?></p>
				<?php endif; ?>
				<?php echo do_shortcode('[gravityform id="'.get_field('form_id').'" title="false" description="false"]') ?>
			</section>

			<?php if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'wysiwyg' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'wysiwyg-2-col' ): 
						get_template_part('template-parts/sections/wysiwyg-two-col');
					elseif( get_row_layout() == 'wysiwyg-2-col-header-footer' ): 
						get_template_part('template-parts/sections/wysiwyg-two-col-header-footer');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'cards' ): 
						get_template_part('template-parts/sections/card-grid');
					elseif( get_row_layout() == 'icons' ): 
						get_template_part('template-parts/sections/icon-slider');
					elseif( get_row_layout() == 'team' ): 
						get_template_part('template-parts/sections/team');
					endif;
				endwhile;
			endif; ?>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>