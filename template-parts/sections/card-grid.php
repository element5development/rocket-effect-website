<?php 
/*----------------------------------------------------------------*\

	SLIDER FOR ICONS 
	commonly used for awards and partners

\*----------------------------------------------------------------*/
?>
<section class="cards">
	<?php if( get_sub_field('section_title') ): ?>
		<h2><?php the_sub_field('section_title'); ?></h2>
	<?php endif; ?>
	<?php if( have_rows('cards') ): ?>
		<div>
			<?php while ( have_rows('cards') ) : the_row(); ?>
				<div class="card">
					<?php $link = get_sub_field('button'); ?>
					<a href="<?php echo $link['url']; ?>"></a>
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
					<a class="button is-borderless is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>