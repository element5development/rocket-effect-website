<?php

/*----------------------------------------------------------------*\
	EXTERNAL JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*----------------------------------------------------------------*\
	APPLY THEME CSS TO EDITOR
\*----------------------------------------------------------------*/
add_editor_style('/assets/styles/main.css');

/*----------------------------------------------------------------*\
	REMOVE H1 OPTION FROM EDITOR
\*----------------------------------------------------------------*/
function remove_h1_from_heading($args) {
	$args['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Pre=pre';
	return $args;
}
add_filter('tiny_mce_before_init', 'remove_h1_from_heading' );

/*----------------------------------------------------------------*\
	BODY CLASSES
\*----------------------------------------------------------------*/
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
	if ( basename(get_page_template()) === 'page.php' && !is_404() ) {
		$classes[] = 'default-template';	
	}
	return $classes;
}
/*----------------------------------------------------------------*\
	ADD EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );