<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<section class="gallery">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<?php if ( get_sub_field('description') ) : ?>
		<p><?php the_sub_field('description'); ?></p>
	<?php endif; ?>
	<div class="gallery-items">
		<?php $images = get_sub_field('gallery'); ?>
		<?php foreach( $images as $image ): ?>
			<a class="gallery-item" href="<?php echo $image['url']; ?>">
				<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		<?php endforeach; ?>
	</div>
</section>