/*----------------------------------------------------------------*\
	Gulp.js configuration
\*----------------------------------------------------------------*/
'use strict';
const
	/*------------------------------*\
		source and build folders
	\*------------------------------*/
	dir = {
		src: 'assets/',
		build: 'dist/'
	},
	/*------------------------------*\
		gulp, plugins, and packages
	\*------------------------------*/
	gulp = require('gulp'),
	newer = require('gulp-newer'),
	imagemin = require('gulp-imagemin'),
	postcss = require('gulp-postcss'),
	cssnano = require('gulp-cssnano'),
	atImport = require("postcss-import"),
	concat = require('gulp-concat'),
	stripdebug = require('gulp-strip-debug'),
	uglify = require('gulp-uglify');

/*------------------------------*\
	image path settings
\*------------------------------*/
const images = {
	src: dir.src + 'images/**/*',
	build: dir.build + 'images/'
};
/*------------------------------*\
	video path settings
\*------------------------------*/
const videos = {
	src: dir.src + 'videos/**/*',
	build: dir.build + 'videos/'
};
/*------------------------------*\
	font path settings
\*------------------------------*/
const fonts = {
	src: dir.src + 'fonts/**/*',
	build: dir.build + 'fonts/'
}
/*----------------------------------------------------------------*\
	Image Processing
\*----------------------------------------------------------------*/
gulp.task('images', () => {
	return gulp.src(images.src)
		.pipe(newer(images.build))
		.pipe(imagemin([
			imagemin.gifsicle({
				interlaced: true
			}),
			imagemin.jpegtran({
				progressive: true
			}),
			imagemin.optipng({
				optimizationLevel: 5
			}),
			imagemin.svgo({
				plugins: [{
						removeViewBox: true
					},
					{
						cleanupIDs: false
					}
				]
			})
		]))
		.pipe(gulp.dest(images.build));
});
/*----------------------------------------------------------------*\
	Video Processing
\*----------------------------------------------------------------*/
gulp.task('videos', () => {
	return gulp.src(videos.src)
		.pipe(gulp.dest(videos.build));
});
/*----------------------------------------------------------------*\
	Font Processing
\*----------------------------------------------------------------*/
gulp.task('fonts', () => {
	return gulp.src(fonts.src)
		.pipe(gulp.dest(fonts.build));
});
/*----------------------------------------------------------------*\
	CSS Settings
\*----------------------------------------------------------------*/
var css = {
	src: dir.src + 'styles/main.css',
	watch: dir.src + 'styles/**/*',
	build: dir.build + 'styles/',
	processors: [
		require('postcss-assets')({
			loadPaths: ['images/'],
			basePath: dir.build,
			baseUrl: 'dist/styles'
		}),
		atImport(),
		require('postcss-nesting')({}),
		require('postcss-custom-media')({}),
		require('autoprefixer'),
		require('css-mqpacker')
	]
};
/*----------------------------------------------------------------*\
	CSS Processing
\*----------------------------------------------------------------*/
gulp.task('css', gulp.series(['images', 'videos', 'fonts'], () => {
	return gulp.src(css.src)
		.pipe(postcss(css.processors))
		.pipe(cssnano({
			discardComments: {
				removeAll: true
			},
			zindex: false
		}))
		.pipe(gulp.dest(css.build))
}));
/*----------------------------------------------------------------*\
	Vendor JS Settings
\*----------------------------------------------------------------*/
const jsVendors = {
	src: dir.src + 'scripts/vendors/**/*',
	build: dir.build + 'scripts/vendors/',
	filename: 'vendors.js'
}
/*----------------------------------------------------------------*\
	Vendor JS Processing
\*----------------------------------------------------------------*/
gulp.task('vendors', () => {
	return gulp.src(jsVendors.src)
		.pipe(concat(jsVendors.filename))
		.pipe(stripdebug())
		.pipe(uglify())
		.pipe(gulp.dest(jsVendors.build))
});
/*----------------------------------------------------------------*\
	Custom JS Settings
\*----------------------------------------------------------------*/
const js = {
	src: dir.src + 'scripts/master/**/*',
	build: dir.build + 'scripts/master/',
	filename: 'main.js'
};
const jsWatch = {
	src: dir.src + 'scripts/**/*',
}
/*----------------------------------------------------------------*\
	Custom JS Processing
\*----------------------------------------------------------------*/
gulp.task('js', gulp.series(['vendors'], () => {
	return gulp.src(js.src)
		.pipe(concat(js.filename))
		.pipe(uglify())
		.pipe(gulp.dest(js.build))
}));
/*----------------------------------------------------------------*\
	Run All Functions Once
\*----------------------------------------------------------------*/
gulp.task('build', gulp.series(['css', 'js']));
/*----------------------------------------------------------------*\
	Watch Files For Changes
\*----------------------------------------------------------------*/
gulp.task('watch', () => {
	gulp.watch(images.src, gulp.series('images'));
	gulp.watch(videos.src, gulp.series('videos'));
	gulp.watch(css.watch, gulp.series('css'));
	gulp.watch(jsWatch.src, gulp.series('js'));
});
/*----------------------------------------------------------------*\
	Run All Functions & Then Watch Files For Additional Changes
\*----------------------------------------------------------------*/
gulp.task('default', gulp.series(['build', 'watch']));