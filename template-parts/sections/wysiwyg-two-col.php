<?php 
/*----------------------------------------------------------------*\

	WYSIWYG

\*----------------------------------------------------------------*/
?>


<section class="wysiwyg-block">
	<div>
		<div>
			<?php the_sub_field('left-wysiwyg'); ?>
		</div>
		<div>
			<?php the_sub_field('right-wysiwyg'); ?>
		</div>
	</div>
</section>