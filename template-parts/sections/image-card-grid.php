<?php 
/*----------------------------------------------------------------*\

	IMAGE CARDS

\*----------------------------------------------------------------*/
?>
<section class="image-cards">
	<?php if( get_sub_field('section_title') ): ?>
		<h2><?php the_sub_field('section_title'); ?></h2>
	<?php endif; ?>
	<?php if( have_rows('image-cards') ): ?>
		<div>
			<?php while ( have_rows('image-cards') ) : the_row(); ?>
				<div class="card">
				<?php $link = get_sub_field('button'); ?>
					<a href="<?php echo $link['url']; ?>"></a>
					<?php $image = get_sub_field('image'); ?>
					<div style="background-image: url('<?php echo $image['sizes']['medium']; ?>');"></div>
					<div>
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<?php if( get_sub_field('button') ): ?>
							<a class="button is-borderless is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>