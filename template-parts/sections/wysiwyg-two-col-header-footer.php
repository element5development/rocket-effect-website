<?php 
/*----------------------------------------------------------------*\

	WYSIWYG

\*----------------------------------------------------------------*/
?>


<section class="wysiwyg-block header-footer">
	<div>
		<h2><?php the_sub_field('wysiwyg-title'); ?></h2>
		<div>
			<?php the_sub_field('left-wysiwyg'); ?>
		</div>
		<div>
			<?php the_sub_field('right-wysiwyg'); ?>
		</div>
		<div>
			<?php the_sub_field('bottom-wysiwyg'); ?>
		</div>
	</div>
</section>