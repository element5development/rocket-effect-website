<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH VIDEO BACKGROUND

\*----------------------------------------------------------------*/
?>


<section class="video-background">
	<?php $image = get_sub_field('image'); ?>
	<?php $video = get_sub_field('video'); ?>
	<div class="video-wrap">
		<video muted="" autoplay="" loop="" class="bgvid" poster="<?php echo get_site_url(); ?><?php echo $image; ?>" data-src="<?php echo get_site_url(); ?><?php echo $video; ?>" src="<?php echo get_site_url(); ?><?php echo $video; ?>"></video>
	</div>
</section>