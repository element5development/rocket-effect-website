<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>

	<article class="default-template">

		<?php if( have_rows('article') ):
			while ( have_rows('article') ) : the_row();
				if( get_row_layout() == 'wysiwyg' ):
					get_template_part('template-parts/sections/wysiwyg');
				elseif( get_row_layout() == 'wysiwyg-2-col' ): 
					get_template_part('template-parts/sections/wysiwyg-two-col');
				elseif( get_row_layout() == 'wysiwyg-2-col-header-footer' ): 
					get_template_part('template-parts/sections/wysiwyg-two-col-header-footer');
				elseif( get_row_layout() == 'banner' ): 
					get_template_part('template-parts/sections/banner');
				elseif( get_row_layout() == 'cards' ): 
					get_template_part('template-parts/sections/card-grid');
				elseif( get_row_layout() == 'image-cards' ): 
					get_template_part('template-parts/sections/image-card-grid');
				elseif( get_row_layout() == 'icon-cards' ): 
					get_template_part('template-parts/sections/icon-card-grid');
				elseif( get_row_layout() == 'icons' ): 
					get_template_part('template-parts/sections/icon-slider');
				elseif( get_row_layout() == 'team' ): 
					get_template_part('template-parts/sections/team');
				elseif( get_row_layout() == 'image-background' ): 
					get_template_part('template-parts/sections/image-background');
				elseif( get_row_layout() == 'video-background' ): 
					get_template_part('template-parts/sections/video-background');
				elseif( get_row_layout() == 'gallery' ): 
					get_template_part('template-parts/sections/gallery');
				endif;
			endwhile;
		endif; ?>

	</article>

</main>

<?php get_template_part('template-parts/sections/related-successstory'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>