<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title has-image">
	<section>
		<div class="title">
			<h1>
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
					else :
						the_title();
					endif;
				?>
			</h1>
			<?php if ( get_field('title_description') ) : ?>
				<p>
					<?php the_field('title_description'); ?>
				</p>
			<?php endif; ?>
		</div>
		<div class="form flip-container">
			<div class="flipper">
				<div class="front">
					<div class="start">
						<h4>I'm ready to launch my marketing to the next level.</h4>
						<p>Talk to a product specialist!</p>
						<?php echo do_shortcode('[gravityform id="'.get_field('form_id').'" title="false" description="false" ajax=true]') ?>
					</div>
					<div class="confirm">
						<h4>We have received your request!</h4>
						<p>A team member will be in touch with you shortly to schedule your demo! </p>
						<img data-emergence="hidden" src="<?php echo get_template_directory_uri();  ?>/dist/images/confirmation.svg" alt="message recieved" />
					</div>
				</div>
				<div class="back">
					<h4>Share a little more.</h4>
					<p>Please provide some additional details to allow our specialist to assist you better.</p>
					<?php echo do_shortcode('[gravityform id="'.get_field('additional_form_id').'" title="false" description="false" ajax=true]') ?>
				</div>
			</div>
		</div>
	</section>

	<div class="gradients"></div>

</header>

<main>

	<article>

		<section class="wysiwyg-block">
			<div>
				<h2><?php the_field('two_col_heading');?></h2>
				<div>
					<?php the_field('left_wysiwyg');?>
				</div>
				<div>
					<?php the_field('right_wysiwyg');?>
				</div>
				<a target="" href="/contact/" class="button is- is- is-">Talk to a product specialist</a>
			</div>
		</section>

		<section class="client-slider">
			<h2><?php the_field('client_heading'); ?></h2>
			<?php if( have_rows('client_icons') ): ?>
			<div class="client-icons">
				<?php while ( have_rows('client_icons') ) : the_row(); ?>
					<div class="icon">
						<?php $image = get_sub_field('icon'); ?>
						<?php $link = get_sub_field('link'); ?>
						<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
							<img src="<?php echo $thumb = $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
						</a>
					</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</section>
		
		<section class="graphic-cta">
			<div>
				<div class="graphic" data-emergence="hidden">
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/grid.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/fun card.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/bottom screen.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/middle screen.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/top screen.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/dist/images/welcome back.png" />
				</div>
				<div>
					<div class="wysiwyg-block">
						<?php the_field('graphic_cta_one');?>
					</div>
				</div>
				<div>
					<div class="wysiwyg-block">
						<?php the_field('graphic_cta_two');?>
					</div>
				</div>
			</div>
		</section>

		<?php if( have_rows('cards') ): ?>
			<section class="cards">
				<div>
					<?php while ( have_rows('cards') ) : the_row(); ?>
						<div class="card">
							<?php $link = get_sub_field('button'); ?>
							<a href="<?php echo $link['url']; ?>"></a>
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('description'); ?></p>
							<a class="button is-borderless is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>

		<section class="banner no-description">
			<div>
				<h2><?php the_field('banner_title'); ?></h2>
				<div>
					<p><?php the_field('banner_description'); ?></p>
				</div>
				<div>
					<?php $link = get_field('banner_button'); ?>
					<a class="button is-ghost is-white" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
				</div>
			</div>
		</section>

		<section class="icon-slider">
			<h2><?php the_field('partner_heading'); ?></h2>
			<?php if( have_rows('partner_icons') ): ?>
			<div class="icons">
				<?php while ( have_rows('partner_icons') ) : the_row(); ?>
					<div class="icon">
						<?php $image = get_sub_field('icon'); ?>
						<img src="<?php echo $thumb = $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</section>

		<?php if( the_field('closing_title') ) : ?>
			<section class="banner has-description">
				<div>
					<h3><?php the_field('closing_title'); ?></h3>
					<div>
						<p><?php the_field('closing_description'); ?></p>
					</div>
					<div>
						<?php $link = get_field('closing_button'); ?>
						<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					</div>
				</div>
			</section>
		<?php endif; ?>
		
	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>