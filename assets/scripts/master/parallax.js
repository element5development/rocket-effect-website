var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			HOME FORM PARALLAX
		\*----------------------------------------------------------------*/
	var rellax = new Rellax('.home .page-title .title', {
		speed: -4,
		center: false,
		wrapper: null,
		round: true,
		vertical: true,
		horizontal: false
	});
	if ($(window).width() < 800) {
		rellax.destroy();
	}
	/*----------------------------------------------------------------*\
		HOME GRAPHIC PARALLAX
	\*----------------------------------------------------------------*/
	var rellax = new Rellax('.home .graphic', {
		speed: -4,
		center: false,
		wrapper: null,
		round: true,
		vertical: true,
		horizontal: false
	});
	if ($(window).width() < 800) {
		rellax.destroy();
	}
});