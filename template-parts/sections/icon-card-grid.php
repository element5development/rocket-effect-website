<?php 
/*----------------------------------------------------------------*\

	ICON CARDS

\*----------------------------------------------------------------*/
?>
<section class="icon-cards">
	<?php if( get_sub_field('section_title') ): ?>
		<h2><?php the_sub_field('section_title'); ?></h2>
	<?php endif; ?>
	<?php if( have_rows('icon-cards') ): ?>
		<div>
			<?php while ( have_rows('icon-cards') ) : the_row(); ?>
				<div class="card">
					<div>
						<?php $image = get_sub_field('icon'); ?>
						<img src="<?php echo $thumb = $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
						<h3><?php the_sub_field('title'); ?></h3>
					</div>
					<p><?php the_sub_field('description'); ?></p>
					<?php if( get_sub_field('button') ): ?>
						<?php $link = get_sub_field('button'); ?>
						<a class="button is-borderless is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>