<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>


<section class="team-members">
	<div class="title">
		<h2><?php the_sub_field('title'); ?></h2>
		<?php $link = get_sub_field('button'); ?>
		<a class="button is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
	</div>
	<?php if( have_rows('team') ): ?>
	<div class="team">
		<?php while ( have_rows('team') ) : the_row(); ?>
			<div class="member">
				<?php $image = get_sub_field('headshot'); ?>
				<img src="<?php echo $thumb = $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
				<p><?php the_sub_field('name'); ?></p>
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</section>