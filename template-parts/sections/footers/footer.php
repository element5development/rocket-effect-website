<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<div>
		<div>
			<a href="<?php echo get_home_url(); ?>">
				<svg>
					<use xlink:href="#logo-red" />
				</svg>
			</a>
		</div>
		<section>
			<h4>Get Pricing</h4>
			<p>Ready to sign up for Rocket Effect?</p>
			<a class="button is-ghost is-purple" href="<?php echo get_site_url(); ?>/pricing/">Get Pricing Information</a>
		</section>
		<section>
			<h4>See a Demo</h4>
			<p>Want to see Rocket Effect in action?</p>
			<a class="button is-ghost is-purple" href="<?php echo get_site_url(); ?>/demo/">Request a Demo</a>
		</section>
		<section>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'footer_nav' )); ?>
			</nav>
		</section>
	</div>
	<div class="copyright">
		<section class="legal">
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
			<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		</section>
	</div>
</footer>