<?php 
/*----------------------------------------------------------------*\

	SLIDER FOR ICONS 
	commonly used for awards and partners

\*----------------------------------------------------------------*/
?>

<section class="icon-slider">
	<h2><?php the_sub_field('title'); ?></h2>
	<?php if( have_rows('icons') ): ?>
	<div class="icons">
		<?php while ( have_rows('icons') ) : the_row(); ?>
			<div class="icon">
				<?php $image = get_sub_field('icon'); ?>
				<img src="<?php echo $thumb = $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</section>