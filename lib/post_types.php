<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Custom Post Type Success Story
// Post Type Key: successstory
function create_successstory_cpt() {

	$labels = array(
		'name' => __( 'Success Stories', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Success Story', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Success Stories', 'textdomain' ),
		'name_admin_bar' => __( 'Success Story', 'textdomain' ),
		'archives' => __( 'Success Story Archives', 'textdomain' ),
		'attributes' => __( 'Success Story Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Success Story:', 'textdomain' ),
		'all_items' => __( 'All Success Stories', 'textdomain' ),
		'add_new_item' => __( 'Add New Success Story', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Success Story', 'textdomain' ),
		'edit_item' => __( 'Edit Success Story', 'textdomain' ),
		'update_item' => __( 'Update Success Story', 'textdomain' ),
		'view_item' => __( 'View Success Story', 'textdomain' ),
		'view_items' => __( 'View Success Stories', 'textdomain' ),
		'search_items' => __( 'Search Success Story', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Success Story', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Success Story', 'textdomain' ),
		'items_list' => __( 'Success Stories list', 'textdomain' ),
		'items_list_navigation' => __( 'Success Stories list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Success Stories list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Success Story', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-portfolio',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'success-stories' ),
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'successstory', $args );

}
add_action( 'init', 'create_successstory_cpt', 0 );