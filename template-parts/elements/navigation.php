<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<div class="navigation-wrap">
	<nav class="navigation-block">
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo-white" />
			</svg>
		</a>
		<nav class="primary">
			<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
		</nav>
		<nav class="primary mobile">
			<?php wp_nav_menu(array( 'theme_location' => 'mobile_nav' )); ?>
		</nav>
		<a href="<?php the_permalink(308); ?>" class="button is-ghost is-white smooth-scroll">
			<span>Request a</span> Demo
		</a>
		<button>
			<div class="hamburger-menu"></div>
		</button>
	</nav>
</div>