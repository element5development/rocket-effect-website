<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>
	<article>
		<section class="form confirmation">
			<?php if ( get_field('message') ) : ?>
				<h2><?php the_field('message'); ?></h2>
			<?php endif; ?>
			<img data-emergence="hidden" src="<?php echo get_template_directory_uri();  ?>/dist/images/confirmation.svg" alt="message recieved" />
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>